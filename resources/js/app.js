import './bootstrap';
import $ from 'jquery';
window.$ = $;

function countUpFromTime(countFrom, id) {
    countFrom = new Date(countFrom).getTime();

    var now = new Date(),
        countFrom = new Date(countFrom),
        timeDifference = (now - countFrom);

    var secondsInADay = 60 * 60 * 1000 * 24,
        secondsInAHour = 60 * 60 * 1000;

    let hours = Math.floor((timeDifference % (secondsInADay)) / (secondsInAHour) * 1).toString().padStart(2, "0");
    let minutes = Math.floor(((timeDifference % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1).toString().padStart(2, "0");
    let seconds = Math.floor((((timeDifference % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1).toString().padStart(2, "0");

    console.log(paused)

    if (paused == false) {
        clearTimeout(countUpFromTime.interval);
        countUpFromTime.interval = setTimeout(function () {
            countUpFromTime(countFrom, id);
        }, 1000);

        $('#' + id).html(hours + ":" + minutes + ":" + seconds)

    }
}

$(document).ready(function () {

    if (start_time != null) {
        countUpFromTime(start_time, 'timer');
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.populate-tasks').on('change', function () {
        let family_id = $(this).val()
        let destination = $(this).data('destination')
        let url = $(this).data('url')

        $(destination).empty();
        $(destination).append('<option>Cargando...</option>');

        $.ajax({
            url: url,
            type: 'POST',
            data: {
                family_id: family_id
            },
            success: function (response) {
                $(destination).empty();
                $(destination).append('<option value="">Seleccione una tarea</option>');
                response.forEach(function (task) {
                    $(destination).append('<option value="' + task.id + '">' + task.name + '</option>');
                }
                )
            }
        });
    });

    $('#new-tracking').on('click', function () {
        $('.tracking-active').slideToggle(100);
        return false;
    })

    $('.tracking-paused').on('click', function () {
        alert('Una tarea pausda no puede ser parada.')
        return false;
    });

});
