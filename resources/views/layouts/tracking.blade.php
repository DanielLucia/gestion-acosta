<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{ seo()->render() }}

    @vite(['resources/css/app.css'])

</head>
<body class="page-{{ Route::current()->getName()  }}">

    @auth

        <header id="header" class="site-header">
            <h1>{{__('Tracking')}}</h1>

            <div class="menu">
                <label class="menu__button" for="menu"><span class="menu__button--dot"></span><span class="menu__button--dot"></span><span class="menu__button--dot"></span></label>
                <input class="menu__switch" type="checkbox" id="menu">
                <ul class="menu__submenu">

                    @if (Auth::user()->hasRole('Admin'))
                        <li><a href="{{ url('dashboard') }}">{{ __('Panel') }}</a></li>
                    @endif

                    <!-- <li><a href="#">{{ __('Mi cuenta') }}</a></li> -->
                    <li><a href="{{ url('logout') }}">{{ __('Salir') }}</a></li>

                </ul>
            </div>

        </header>

        <div class="user">
            <div class="user__label">{{ __('Trabajador/a') }}</div>
            <div class="user__name">{{ auth()->user()->name }}</div>
        </div>

    @endauth

    <div class="content">
        @yield('content')
    </div>

    @auth

        <footer class="site-fotoer">

        </footer>

    @endauth

    @vite(['resources/js/app.js'])

    <script>
        let start_time = @if (isset($actual_task))"{{ $actual_task->time_for_timer }}" @else null @endif;
    </script>

</body>
</html>
