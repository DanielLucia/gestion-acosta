<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{ seo()->render() }}

    @vite(['resources/css/app.css'])

</head>
<body class="page-{{ Route::current()->getName()  }}">

    <header id="header" class="site-header">
        <h1>{{__('Panel de administración')}}</h1>
    </header>

    <div class="dashboard">
        <nav class="dashboard__menu">

            <div class="dashboard__title">
                <label class="dashboard__title--label" for="menu">{{__('Menú')}}</label>
            </div>

            <input type="checkbox" class="dashboard__switch" id="menu">
            <ul class="dashboard__list dashboard__element-switch">
                <li><a href="{{ route('users') }}" class="dashboard__list--element link-icon link-icon--users {{ (request()->routeIs('users*')) ? 'active' : '' }}">{{__('Trabajadores')}}</a></li>
                <li><a href="{{ route('families') }}" class="dashboard__list--element link-icon link-icon--families {{ (request()->routeIs('families*')) ? 'active' : '' }}">{{__('Familias')}}</a></li>
                <li><a href="{{ route('products') }}" class="dashboard__list--element link-icon link-icon--products {{ (request()->routeIs('products*')) ? 'active' : '' }}">{{__('Productos')}}</a></li>
                <li><a href="{{ route('tasks') }}" class="dashboard__list--element link-icon link-icon--tasks {{ (request()->routeIs('tasks*')) ? 'active' : '' }}">{{__('Tareas')}}</a></li>
                <li><a href="{{ route('logout') }}" class="dashboard__list--element link-icon link-icon--logout">{{__('Salir')}}</a></li>
            </ul>
        </nav>

        @yield('content')

    </div>

    @if(Session::has('message'))
        <div class="message">{{ session('message') }}</div>
    @endif

</body>
</html>
