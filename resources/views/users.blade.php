@extends('layouts.admin')

@section('content')

    <section class="dashboard__items">
        <div class="dashboard__title">
            <label class="dashboard__title--label" for="menu">{{__('Usuarios')}}</label>
            <a href="{{ route('users.create') }}" class="dashboard__title--link form__button">{{__('Nuevo usuario')}}</a>
        </div>

        <input type="checkbox" class="dashboard__switch" id="items">
        <ul class="dashboard__list dashboard__element-switch">

            @foreach ($users as $user)
                <li>
                    <a href="{{ route('users.edit', $user) }}" class="dashboard__list--element @if (isset($userToEdit) && $userToEdit->id == $user->id) active @endif">
                        <strong>{{ $user->name }}</strong>
                        <span class="dashboard__list--label">{{ $user->email }}</span>
                        <span class="dashboard__list--label">{{ $user->hours_per_month }} h.</span>
                        <span class="dashboard__list--label">{{ $user->salary }} €</span>
                    </a>
                </li>
            @endforeach

        </ul>
    </section>

    @if ( request()->routeIs('users.create') or request()->routeIs('users.edit') )

        <section class="dashboard__item">

            <div class="dashboard__title">
                <label class="dashboard__title--label" for="menu">{{__('Nuevo trabajador')}}</label>

                @if (isset($userToEdit))
                    <a href="{{ route('users.view', $userToEdit) }}" class="dashboard__title--link form__button">{{__('Histórico')}}</a>
                @endif

                @if (isset($userToEdit) && $userToEdit->id != auth()->user()->id)
                    <a onclick="return confirm('¿Estás seguro?')" href="{{ route('users.delete', $userToEdit) }}" class="dashboard__title--link form__button form__button--cancel">{{__('Borrar')}}</a>
                @endif
            </div>

            <input type="checkbox" class="dashboard__switch" id="item">

            <form class="form dashboard__element-switch" method="post" action="{{ route('users.save') }}">

                @csrf

                <div class="form__group">
                    <input type="text" name="name" id="name" class="form__input" placeholder="{{ __('Nombre') }}" required autofocus @if (isset($userToEdit)) value="{{ $userToEdit->name }}" @endif />
                </div>

                <div class="form__group">
                    <input type="email" name="email" id="email" class="form__input" placeholder="{{ __('Email') }}" required @if (isset($userToEdit)) value="{{ $userToEdit->email }}" @endif />
                </div>

                <div class="form__group">
                    <input type="password" name="password" id="password" class="form__input" placeholder="{{ __('Contraseña') }}" />
                </div>

                <div class="form__group">
                    <input type="text" name="hours_per_month" id="hours_per_month" class="form__input" placeholder="{{ __('Horas mensuales') }}" required @if (isset($userToEdit)) value="{{ $userToEdit->hours_per_month }}" @endif />
                </div>

                <div class="form__group">
                    <input type="text" name="salary" id="salary" class="form__input" placeholder="{{ __('Salario') }}" required @if (isset($userToEdit)) value="{{ $userToEdit->salary }}" @endif />
                </div>

                <div class="form__group">
                    <select name="role" id="role" class="form__input" required>

                        @foreach ($roles as $role)
                            <option value="{{ $role->name }}" @if (isset($userToEdit) && $userToEdit->hasRole($role->name)) selected @endif>{{ $role->name }}</option>
                        @endforeach

                    </select>
                </div>

                <div class="form__group">
                    <button type="submit" class="form__button">{{ __('Guardar') }}</button>
                </div>

                @if (isset($userToEdit))
                    <input type="hidden" name="id" value="{{ $userToEdit->id }}">
                @endif

            </form>



        </section>

    @endif

@endsection
