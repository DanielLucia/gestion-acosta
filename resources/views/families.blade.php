@extends('layouts.admin')

@section('content')

    <section class="dashboard__items">
        <div class="dashboard__title">
            <label class="dashboard__title--label" for="menu">{{__('Familia de productos')}}</label>
            <a href="{{ route('families.create') }}" class="dashboard__title--link form__button">{{__('Nueva familia')}}</a>
        </div>

        <input type="checkbox" class="dashboard__switch" id="items">
        <ul class="dashboard__list dashboard__element-switch">
            @foreach ($families as $family)
                <li>
                    <a href="{{ route('families.edit', $family) }}" class="dashboard__list--element">
                        <strong>{{ $family->name }}</strong>
                    </a>
                </li>
            @endforeach
        </ul>
    </section>

    @if ( request()->routeIs('families.create') or request()->routeIs('families.edit') )

        <section class="dashboard__item">

            <div class="dashboard__title">
                <label class="dashboard__title--label" for="menu">{{__('Nueva familia')}}</label>

                @if (isset($familyToEdit) )
                    <a onclick="return confirm('¿Estás seguro?')" href="{{ route('families.delete', $familyToEdit) }}" class="dashboard__title--link form__button form__button--cancel">{{__('Borrar')}}</a>
                @endif

            </div>

            <input type="checkbox" class="dashboard__switch" id="item">
            <form class="form dashboard__element-switch" method="post" action="{{ route('families.save') }}">
                @csrf
                <div class="form__group">
                    <input type="text" name="name" id="name" class="form__input" placeholder="{{ __('Nombre') }}" autofocus @if (isset($familyToEdit)) value="{{ $familyToEdit->name }}" @endif />
                </div>

                <div class="form__group">
                    <button type="submit" class="form__button">{{ __('Guardar') }}</button>
                </div>

                @if (isset($familyToEdit))
                    <input type="hidden" name="id" value="{{ $familyToEdit->id }}">
                @endif

            </form>

        </section>

    @endif

@endsection
