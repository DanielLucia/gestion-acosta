@extends('layouts.admin')

@section('content')

    <section class="dashboard__items">
        <div class="dashboard__title">
            <label class="dashboard__title--label" for="menu">{{__('Usuarios')}}</label>
            <a href="{{ route('users.create') }}" class="dashboard__title--link form__button">{{__('Nuevo usuario')}}</a>
        </div>

        <input type="checkbox" class="dashboard__switch" id="items">
        <ul class="dashboard__list dashboard__element-switch">

            @foreach ($users as $user)
                <li>
                    <a href="{{ route('users.edit', $user) }}" class="dashboard__list--element @if (isset($userToEdit) && $userToEdit->id == $user->id) active @endif">
                        <strong>{{ $user->name }}</strong>
                        <span class="dashboard__list--label">{{ $user->email }}</span>
                        <span class="dashboard__list--label">{{ $user->hours_per_month }} h.</span>
                        <span class="dashboard__list--label">{{ $user->salary }} €</span>
                    </a>
                </li>
            @endforeach

        </ul>
    </section>


        <section class="dashboard__item">

            <form class="dashboard__title dashboard__title--form" method="get" action="{{ route('users.view', $userToEdit) }}">
                <label class="dashboard__title--label" for="menu">{{__('Histórico')}}</label>

                <label>{{__('Desde')}}</label>
                <input type="date" class="form__input" id="date_from" name="date_from" value="{{ request()->get('date_from') }}" placeholder="{{ __('Desde') }}">

                <label>{{__('Hasta')}}</label>
                <input type="date" class="form__input" id="date_to" name="date_to" value="{{ request()->get('date_to') }}" placeholder="{{ __('Hasta') }}">

                <select class="form__select populate-tasks" id="family" name="family">
                    <option value="">{{ __('Seleccione familia') }}</option>
                    @foreach ($families as $family)
                        <option value="{{$family->id}}" @if (request()->get('family') == $family->id) selected @endif>{{ $family->name }}</option>
                    @endforeach
                </select>

                <button type="submit" class="dashboard__title--link form__button form__button--success">{{__('Filtrar')}}</button>

                <input type="submit" class="dashboard__title--link form__button form__button--warning" name="export" value="{{__('Exportar')}}"></button>

                <a href="{{ route('users.edit', $userToEdit) }}" class="dashboard__title--link form__button">{{__('Volver')}}</a>

            </form>

            <input type="checkbox" class="dashboard__switch" id="item">

            <h1 class="trackings__title">{{ $userToEdit->name }}</h1>

            @if ($trackings)

                <ul class="dashboard__list dashboard__element-switch dashboard__timers">

                    @foreach($trackings as $tracking)

                        <li>
                            <span class="dashboard__list--element">
                                <span class="dashboard__list--label">{{ $tracking->id }}</span>
                                <strong>{{ $tracking->task->family->name ?? 'Sin familia' }}</strong>
                                <span class="dashboard__list--label"><strong>{{ $tracking->task->name  ?? 'Sin tarea' }}</strong></span>
                                <span class="dashboard__list--label">{{ gmdate("H:i:s", $tracking->time) }}</span>
                            </span>
                        </li>

                    @endforeach

                    <li>
                        <span class="dashboard__list--element">
                            <strong>Total</strong>
                            <span class="dashboard__list--label">{{ gmdate("H:i:s", $total) }}</span>
                        </span>
                    </li>

                </ul>

            @endif


        </section>

@endsection
