@extends('layouts.tracking')

@section('content')

    <div class="login">
        <div class="logo">
            <img src="{{ asset('images/logo.png') }}" alt="Logo" loading="lazy">
        </div>

        <div class="window">
            <div class="window__header">{{ __('Acceso trabajador') }}</div>
            <div class="window__content">
                <form class="form" action="{{ route('login') }}" method="post">

                    @csrf

                    @if ($errors->has('email'))
                        <span class="form__message">{{ $errors->first('email') }}</span>
                    @endif

                    <div class="form__group">
                        <!-- <label for="email" class="form__label">{{ __('Correo electrónico') }}</label> -->
                        <input type="email" name="email" id="email" class="form__input" placeholder="{{ __('Correo electrónico') }}" required />
                    </div>

                    <div class="form__group">
                        <!-- <label for="password" class="form__label">{{ __('Contraseña') }}</label> -->
                        <input type="password" name="password" id="password" class="form__input" placeholder="{{ __('Contraseña') }}" required />
                    </div>

                    <div class="form__group">
                        <button type="submit" class="form__button">{{ __('Entrar') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
