@extends('layouts.admin')

@section('content')

    <section class="dashboard__items">
        <div class="dashboard__title">
            <label class="dashboard__title--label" for="menu">{{__('Tareas')}}</label>
            <a href="{{ route('tasks.create') }}" class="dashboard__title--link form__button">{{__('Nueva tarea')}}</a>
        </div>

        <input type="checkbox" class="dashboard__switch" id="items">
        <ul class="dashboard__list dashboard__element-switch">
            @foreach ($tasks as $task)
                <li>
                    <a href="{{ route('tasks.edit', $task) }}" class="dashboard__list--element">
                        <strong>{{ $task->name }}</strong>
                        <span class="dashboard__list--label">{!! $task->family->name ?? '<em>Sin familia</em>' !!}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </section>

    @if ( request()->routeIs('tasks.create') or request()->routeIs('tasks.edit') )

        <section class="dashboard__item">

            <div class="dashboard__title">
                <label class="dashboard__title--label" for="menu">{{__('Nueva tarea')}}</label>

                @if (isset($taskToEdit) )
                    <a onclick="return confirm('¿Estás seguro?')" href="{{ route('tasks.delete', $taskToEdit) }}" class="dashboard__title--link form__button form__button--cancel">{{__('Borrar')}}</a>
                @endif

            </div>

            <input type="checkbox" class="dashboard__switch" id="item">
            <form class="form dashboard__element-switch" method="post" action="{{ route('tasks.save') }}">
                @csrf

                <div class="form__group">
                    <select name="family_id" id="family_id" class="form__input">

                    <option value="0">{{ __('Sin familia' ) }}</option>
                    @foreach ($families as $family)
                        <option value="{{$family->id}}" @if (isset($taskToEdit) && $taskToEdit->family_id == $family->id ) selected @endif >{{ $family->name }}</option>
                    @endforeach

                    </select>
                </div>

                <div class="form__group">
                    <input type="text" name="name" id="name" class="form__input" placeholder="{{ __('Nombre') }}" autofocus @if (isset($taskToEdit)) value="{{ $taskToEdit->name }}" @endif />
                </div>

                <div class="form__group">
                    <button type="submit" class="form__button">{{ __('Guardar') }}</button>
                </div>

                @if (isset($taskToEdit))
                    <input type="hidden" name="id" value="{{ $taskToEdit->id }}">
                @endif

            </form>

        </section>

    @endif

@endsection
