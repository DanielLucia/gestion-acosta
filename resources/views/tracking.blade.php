@extends('layouts.tracking')

@section('content')

    <form class="form @if ($actual_task != null) tracking-active @endif" method="post" action="{{ route('tracking.save') }}" >
        @csrf

        <div class="block">
            <h2 class="block__header">{{__('Seleccionar proceso')}}</h2>
            <div class="block__content">
            <select class="form__select populate-tasks" id="family_id" name="family_id" data-url="{{ route('tasks.json') }}" data-destination="#task_id" required>
                <option value="">{{ __('Seleccione proceso') }}</option>
                @foreach ($families as $family)
                    <option value="{{$family->id}}" @if ($actual_task && $actual_task->family_id == $family->id) selected @endif>{{ $family->name }}</option>
                @endforeach
            </select>
            </div>
        </div>

        <div class="block">
            <h2 class="block__header">{{__('Tarea a realizar')}}</h2>
            <div class="block__content">
            <select class="form__select" id="task_id" name="task_id" required>
                <option value="">{{ __('Debe seleccionar proceso') }}</option>
            </select>
            </div>
        </div>

        <div class="block">
            <div class="block__content">
            <button class="form__button form_button--start">{{ __('Iniciar') }}</button>
            </div>

            <input type="hidden" name="type" value="start" />
        </div>
    </form>

    @if ($actual_task)
    <form class="form" method="post" action="{{ route('tracking.save') }}">
        @csrf

        @if (!$actual_task)
        <div class="block">
            <h2 class="block__header">{{__('Seleccionar proceso')}}</h2>
            <div class="block__content">
                <select class="form__select populate-tasks" id="family_id" name="family_id" data-url="{{ route('tasks.json') }}" data-destination="#task_id" required>
                    <option value="">{{ __('Seleccione proceso') }}</option>
                    @foreach ($families as $family)
                        <option value="{{$family->id}}" @if ($actual_task && $actual_task->family_id == $family->id) selected @endif>{{ $family->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="block">
            <h2 class="block__header">{{__('Tarea a realizar')}}</h2>
            <div class="block__content">
                <select class="form__select" id="task_id" name="task_id" required>
                    <!-- @foreach ($tasks as $task)
                        <option value="{{$task->id}}" @if ($actual_task && $actual_task->task_id == $task->id) selected @endif>{{ $task->name }}</option>
                    @endforeach -->
                </select>
            </div>
        </div>
        @endif

        <div class="block">

            <div class="block__content">
                @if (!$actual_task)

                    <button class="form__button form_button--start">{{ __('Iniciar') }}</button>

                @else

                    <button class="form__button form_button--stop @if ($actual_task != null && $actual_task->paused) tracking-paused @endif">{{ __('Parar') }}</button>

                    <input type="hidden" name="id" value="{{ $actual_task->id }}" />

                    @if ($actual_task != null && $actual_task->paused)
                        <div class="timer" id="timer-paused">{{ gmdate("H:i:s", $actual_task->time) }}</div>
                    @else
                        <div class="timer" id="timer"></div>
                    @endif

                    <div class="timer__info">
                        <strong>{{ __('Tarea actual:') }}</strong>
                        <span>{{ $actual_task->task->family->name }}</span>
                        <em>{{ $actual_task->task->name }}</em>
                    </div>

                @endif
            </div>

            <input type="hidden" name="type" value="@if (!$actual_task)start @else stop @endif" />

        </div>
    </form>
    @endif

    @if ($actual_task != null)

        <div class="timer__buttons">

            <form class="form form--row" method="post" action="{{ route('tasks.details') }}">
                @csrf
                @if ($actual_task->paused)
                    <button class="form__button timer__button--resume no-wrap" id="resume">{{ __('Reanudar') }}</button>
                    <button class="form__button timer__button--new no-wrap" id="new-tracking">{{ __('Nueva tarea') }}</button>
                @else
                    <button class="form__button timer__button--pause" id="pause">{{ __('Pausar') }}</button>
                @endif

                <input type="hidden" name="id" value="{{ $actual_task->id }}" />
            </form>

        </div>

        <script>  const paused = @if ($actual_task->paused) true @else false @endif; </script>

    @endif

    @if (!empty($paused_trackings) && count($paused_trackings) > 0)

        <h4 class="dashboard__title" style="margin-top: 25px;">{{__('Procesos pausados')}}</h4>

        <ul class="dashboard__list dashboard__list--paused-trackings" style="padding-bottom: 25px;">
            @foreach ($paused_trackings as $tracking)
                <li>

                    <strong class="dashboard__list--label">{{ $tracking->task->family->name }}<br />{{ $tracking->task->name }}</strong>
                    <span class="dashboard__list--label"><small>{{ __('Tiempo') }}</small><br />{{ gmdate("H:i:s", $tracking->time) }}</span>
                    <span class="dashboard__list--label"><small>{{ __('Inicio') }}</small><br />{{ date('H:i:s', strtotime($tracking->start_time)) }}</span>
                    <a href="{{ route('tracking', ['id' => $tracking->id]) }}" class="dashboard__title--link form__button">{{__('Reanudar')}}</a>

                </li>
            @endforeach
        </ul>
    @endif

@endsection
