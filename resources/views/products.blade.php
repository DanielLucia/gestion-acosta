@extends('layouts.admin')

@section('content')

    <section class="dashboard__items">
        <div class="dashboard__title">
            <label class="dashboard__title--label" for="menu">{{__('Familia de productos')}}</label>
            <a href="{{ route('products.create') }}" class="dashboard__title--link form__button">{{__('Nuevo producto')}}</a>
        </div>

        <input type="checkbox" class="dashboard__switch" id="items">
        <ul class="dashboard__list dashboard__element-switch">
            @foreach ($products as $product)
                <li>
                    <a href="{{ route('products.edit', $product) }}" class="dashboard__list--element">
                        <strong>{{ $product->name }}</strong>
                    </a>
                </li>
            @endforeach
        </ul>
    </section>

    @if ( request()->routeIs('products.create') or request()->routeIs('products.edit') )

        <section class="dashboard__item">

            <div class="dashboard__title">
                <label class="dashboard__title--label" for="menu">{{__('Nuevo producto')}}</label>

                @if (isset($productToEdit) )
                    <a onclick="return confirm('¿Estás seguro?')" href="{{ route('products.delete', $productToEdit) }}" class="dashboard__title--link form__button form__button--cancel">{{__('Borrar')}}</a>
                @endif

            </div>

            <input type="checkbox" class="dashboard__switch" id="item">
            <form class="form dashboard__element-switch" method="post" action="{{ route('products.save') }}">
                @csrf
                <div class="form__group">
                    <input type="text" name="name" id="name" class="form__input" placeholder="{{ __('Nombre') }}" autofocus @if (isset($productToEdit)) value="{{ $productToEdit->name }}" @endif />
                </div>

                <div class="form__group">
                    <select name="family_id" id="family_id" class="form__input">

                    @foreach ($families as $family)
                        <option value="{{$family->id}}" @if (isset($productToEdit) && $productToEdit->family_id == $family->id ) selected @endif >{{ $family->name }}</option>
                    @endforeach

                    </select>
                </div>

                <div class="form__group">
                    <button type="submit" class="form__button">{{ __('Guardar') }}</button>
                </div>

                @if (isset($productToEdit))
                    <input type="hidden" name="id" value="{{ $productToEdit->id }}">
                @endif

            </form>

        </section>

    @endif

@endsection
