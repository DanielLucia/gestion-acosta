<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\TrackingController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\FamiliesController;

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

/**
 * Usuarios autenticados
 */
Route::middleware(['auth'])->group(function () {

    Route::get('/close-trackings', [TrackingController::class, 'close'])->name('close.tracking');
    Route::get('/tracking', [TrackingController::class, 'index'])->name('tracking');
    Route::post('/tracking/save', [TrackingController::class, 'save'])->name('tracking.save');

    Route::post("/dashboard/tasks/json", ['App\Http\Controllers\\TasksController', 'json'])->name("tasks.json");
    Route::post("/dashboard/tasks/details", ['App\Http\Controllers\\TasksController', 'details'])->name("tasks.details");

    /**
     * Administradores
     */
    Route::group(['middleware' => ['role:Admin']], function () {

        Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
        Route::get('/dashboard/tasks', [TasksController::class, 'index'])->name('tasks');

        foreach (['products', 'families', 'tasks', 'users'] as $resource) {

            $singular = Str::singular($resource);

            Route::get("/dashboard/{$resource}", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'create'])->name($resource);
            Route::get("/dashboard/{$resource}/create", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'create'])->name("{$resource}.create");
            Route::get("/dashboard/{$resource}/{". $singular . "}/delete", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'delete'])->name("{$resource}.delete");
            Route::get("/dashboard/{$resource}/{". $singular . "}", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'edit'])->name("{$resource}.edit");
            Route::post("/dashboard/{$resource}/save", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'save'])->name("{$resource}.save");

            if('user' == $singular) {
                Route::get("/dashboard/{$resource}/{". $singular . "}/view", ['App\Http\Controllers\\' . ucfirst($resource) . 'Controller', 'view'])->name("{$resource}.view");
            }

        }
    });
});
