<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'family_id'];

    public function family(): HasOne
    {
        return $this->hasOne(ProductFamily::class, 'id', 'family_id');
    }
}
