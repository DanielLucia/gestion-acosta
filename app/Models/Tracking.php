<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Tracking extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'task_id', 'product_id', 'start_time', 'end_time', 'paused'];

    public function task(): HasOne
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getTimeForTimerAttribute(): string {
        $start_time = Carbon::parse($this->attributes['start_time']);

        /** Obtenemos tiempo pausado */
        $pauses_total = 0;
        $pauses = TrackingDetails::where('tracking_id', $this->id)
            ->whereNotNull('pause_time')
            ->whereNotNull('resume_time')
            ->get();
        foreach ($pauses as $pause) {
            $pause_time = strtotime($pause->pause_time);
            $resume_time = strtotime($pause->resume_time);
            $pauses_total += $resume_time - $pause_time;
        }

        $response = $start_time->addSeconds($pauses_total)->format('Y-m-d H:i:s');

        return $response;
    }

    public function getTimeAttribute(): int
    {
        $total = 0;

        /**
         * Obtenemos tiempo total
         */
        $start = strtotime($this->start_time);
        $end = strtotime($this->end_time);

        if ($this->paused) {
            $pause = TrackingDetails::where('tracking_id', $this->id)
                ->whereNotNull('pause_time')
                ->whereNull('resume_time')
                ->orderBy('id', 'desc')
                ->first();

            if ($pause) {
                $end = strtotime($pause->pause_time);
            }

        }

        $total = $end - $start;

        /**
         * Restamos tiempos de pausa
         */
        $pauses_total = 0;
        $pauses = TrackingDetails::where('tracking_id', $this->id)
            ->whereNotNull('pause_time')
            ->whereNotNull('resume_time')
            ->get();
        foreach ($pauses as $pause) {
            $pause_time = strtotime($pause->pause_time);
            $resume_time = strtotime($pause->resume_time);
            $pauses_total += $resume_time - $pause_time;
        }

        return $total - $pauses_total;
    }
}
