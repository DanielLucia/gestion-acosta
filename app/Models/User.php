<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'hours_per_month',
        'salary'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function trackings()
    {
        $request = request();
        $instance = $this->hasMany(Tracking::class);

        if (!empty($request->all())) {
            $instance->select('trackings.*');
        }

        if ($request->family) {
            $instance->join('tasks', 'trackings.task_id', '=', 'tasks.id');
            $instance->where('tasks.family_id', $request->family);
        }

        if ($request->date_from) {
            $instance->where('trackings.created_at', '>=', $request->date_from . ' 00:00:00');
        }

        if ($request->date_to) {
            $instance->where('trackings.created_at', '<=', $request->date_to . ' 00:00:00');
        }

        return $instance;
    }
}
