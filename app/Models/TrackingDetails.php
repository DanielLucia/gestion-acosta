<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingDetails extends Model
{
    use HasFactory;
    protected $fillable = ['tracking_id', 'pause_time', 'resume_time'];
}
