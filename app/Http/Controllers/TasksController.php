<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\ProductFamily;
use App\Models\TrackingDetails;
use App\Models\Tracking;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TasksController extends Controller
{
    private $tasks;
    private $families;

    public function __construct()
    {
        $this->tasks = Task::all();
        $this->families = ProductFamily::all();
    }

    public function json(Request $request)
    {
        $tasks = Task::whereFamilyId((int) $request->family_id)->get();

        return response()->json($tasks);
    }

    public function index()
    {
        seo()->title('Gestión Acosta');

        return view('tasks', [
            'families' => $this->families,
            'tasks' => $this->tasks,
        ]);
    }

    public function create()
    {
        seo()->title('Gestión Acosta');

        return view('tasks', [
            'families' => $this->families,
            'tasks' => $this->tasks,
        ]);
    }

    public function edit(Task $task)
    {
        seo()->title('Gestión Acosta');

        return view('tasks', [
            'families' => $this->families,
            'tasks' => $this->tasks,
            'taskToEdit' => $task,
        ]);
    }

    public function delete(Task $task)
    {
        $task->delete();

        return redirect(route('tasks'))->with('message', 'Se ha borrado con éxito con éxito');
    }

    public function save(Request $request)
    {

        if ($request->id) {
            $family = Task::find($request->id);
        } else {
            $family = new Task;
        }

        $family->fill([
            'name' => $request->name,
            'family_id' => $request->family_id,
        ]);

        $family->save();

        return redirect()->back()->with('message', 'Se ha guardado con éxito');
    }

    public function details(Request $request)
    {
        $tracking = Tracking::find($request->id);

        $trackingDetail = TrackingDetails::where('tracking_id', $request->id)
            ->whereNull('pause_time')
            ->whereNull('resume_time')
            ->orderBy('created_at', 'desc')->first();

        /**
         * Si el detalle no tiene fecha ni de pasusa ni de resume,
         * entonces se crea un nuevo detalle
         */
        if ($trackingDetail) {
            $trackingDetail->fill([
                'tracking_id' => (int) $request->id,
                'pause_time' => Carbon::now(),
            ]);


            $trackingDetail->save();

            /** Pausamos tracking */
            $tracking->paused = true;
            $tracking->save();

            return redirect()->back();
        }

        $trackingDetail = TrackingDetails::where('tracking_id', $request->id)
            ->whereNotNull('pause_time')
            ->whereNull('resume_time')
            ->orderBy('created_at', 'desc')->first();
        /**
         * Si el detalle tiene fecha de pausa,
         * pero no de resume, reanudamos el tracking
         */
        if ($trackingDetail) {
            $trackingDetail->fill([
                'tracking_id' => (int) $request->id,
                'resume_time' => Carbon::now(),
            ]);


            $trackingDetail->save();

            /** Reanudamos tracking */
            $tracking->paused = false;
            $tracking->save();

            return redirect()->back();
        }

        $trackingDetail = TrackingDetails::where('tracking_id', $request->id)
            ->whereNotNull('pause_time')
            ->whereNotNull('resume_time')
            ->orderBy('created_at', 'desc')->first();
        /**
         * Si el detalle tiene las dos fechas rellenas,
         * creamos un nuevo detalle pausado
         */
        if ($trackingDetail) {
            $trackingDetail = new TrackingDetails;
            $trackingDetail->fill([
                'tracking_id' => (int) $request->id,
                'pause_time' => Carbon::now(),
            ]);

            $trackingDetail->save();

            /** Pausamos tracking */
            $tracking->paused = true;
            $tracking->save();

            return redirect()->back();
        }
    }
}
