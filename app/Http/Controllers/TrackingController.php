<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductFamily;
use App\Models\Task;
use App\Models\Tracking;
use App\Models\TrackingDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrackingController extends Controller
{

    private $products;
    private $tasks;
    private $families;
    private $paused_trackings;

    function __construct()
    {
        $this->products = Product::all();
        $this->tasks = Task::all();
        $this->families = ProductFamily::all();
        $this->paused_trackings = [];
    }

    public function index(Request $request)
    {

        $actual_task = null;

        if ($request->id && $request->id > 0) {
            $actual_task = Tracking::where('user_id', Auth()->user()->id)
                ->where('id', $request->id)
                ->first();
        }

        $this->paused_trackings = Tracking::wherePaused(true)->whereUserId(Auth()->user()->id)->get();

        return view('tracking', [
            'products' => $this->products,
            'families' => $this->families,
            'tasks' => $this->tasks,
            'actual_task' => $actual_task,
            'paused_trackings' => $this->paused_trackings,
        ]);
    }

    public function save(Request $request)
    {

        if ($request->id) {
            $tracking = Tracking::find($request->id);
        } else {
            $tracking = new Tracking;
            $tracking->user_id = Auth()->user()->id;
            $tracking->task_id = (int)$request->task_id;
            $tracking->product_id = (int)$request->product_id;
        }

        if ($request->type == 'start') {
            $tracking->start_time = Carbon::now();
        } else {
            $tracking->paused = false;
            $tracking->end_time = Carbon::now();
        }

        $tracking->save();

        $id = $tracking->id;

        /**
         * Cuando empezmaos una tarea,
         * creamos un nuevo detalle vacio
         */
        if ($request->type == 'start') {
            $tracking_detail = new TrackingDetails;
            $tracking_detail->tracking_id = $tracking->id;
            $tracking_detail->save();
        } else {
            /**
             * Cuando terminamos una tarea,
             * buscamos el ultimo detalle y comprobamos que tenga fecha de resume
             */
            $tracking_detail = TrackingDetails::where('tracking_id', $tracking->id)->orderBy('created_at', 'desc')->first();

            if ($tracking_detail->resume_time == null) {
                $tracking_detail->resume_time = Carbon::now();
                $tracking_detail->save();
            }

            $id = 0;
        }

        return redirect(route('tracking', ['id' => $id]))->with('message', 'Se ha guardado con éxito');
    }

    public function close(Request $request)
    {
        $hora_maxima = 14;

        if (date('H') < $hora_maxima) {
            return response()->json([
                'response' => 'error',
                'message' => sprintf('No puedes cerrar el tracking antes de las %d:00', $hora_maxima),
            ]);
        }


        $trackings = Tracking::wherePaused(true)->get();
        foreach ($trackings as $tracking) {

            $tracking_detail = TrackingDetails::where('tracking_id', $tracking->id)->orderBy('created_at', 'desc')->first();
            $tracking->paused = false;
            $tracking->end_time = $tracking_detail->pause_time; //Fecha de la última pausa
            $tracking->save();
        }

        return response()->json([
            'response' => 'ok',
            'message' => 'Se ha cerrado el tracking con éxito'
        ]);
    }
}
