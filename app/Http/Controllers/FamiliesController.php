<?php

namespace App\Http\Controllers;

use App\Models\ProductFamily;
use Illuminate\Http\Request;

class FamiliesController extends Controller
{
    private $families;


    public function __construct()
    {
        $this->families = ProductFamily::all();
    }

    public function index()
    {
        seo()->title('Gestión Acosta');

        return view('families', [
            'families' => $this->families,
        ]);
    }

    public function create()
    {
        seo()->title('Gestión Acosta');

        return view('families', [
            'families' => $this->families,
        ]);
    }

    public function edit(ProductFamily $family)
    {
        seo()->title('Gestión Acosta');

        return view('families', [
            'families' => $this->families,
            'familyToEdit' => $family,
        ]);
    }

    public function delete(ProductFamily $family)
    {
        $family->delete();

        return redirect(route('families'))->with('message', 'Se ha borrado con éxito con éxito');
    }

    public function save(Request $request)
    {

        if ($request->id) {
            $family = ProductFamily::find($request->id);
        } else {
            $family = new ProductFamily;
        }

        $family->fill([
            'name' => $request->name,
        ]);

        $family->save();

        return redirect()->back()->with('message', 'Se ha guardado con éxito');
    }
}
