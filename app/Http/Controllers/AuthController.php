<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            Auth::user()->hasRole('Admin') ? redirect(route('dashboard')) : redirect(route('tracking'));

        }

        return back()->withErrors([
            'email' => 'La combinación de email contraseña no es correcta.',
        ])->onlyInput('email');
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('home'));
    }
}
