<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductFamily;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $products;
    private $families;


    public function __construct()
    {
        $this->products = Product::all();
        $this->families = ProductFamily::all();
    }

    public function index()
    {
        seo()->title('Gestión Acosta');

        return view('products', [
            'products' => $this->products,
        ]);
    }

    public function create()
    {
        seo()->title('Gestión Acosta');

        return view('products', [
            'products' => $this->products,
            'families' => $this->families,
        ]);
    }

    public function edit(Product $product)
    {
        seo()->title('Gestión Acosta');

        return view('products', [
            'products' => $this->products,
            'families' => $this->families,
            'productToEdit' => $product,
        ]);
    }

    public function delete(Product $product)
    {
        $product->delete();

        return redirect(route('products'))->with('message', 'Se ha borrado con éxito con éxito');
    }

    public function save(Request $request)
    {

        if ($request->id) {
            $family = Product::find($request->id);
        } else {
            $family = new Product;
        }

        $family->fill([
            'name' => $request->name,
            'family_id' => (int)$request->family_id,
        ]);

        $family->save();

        return redirect()->back()->with('message', 'Se ha guardado con éxito');
    }
}
