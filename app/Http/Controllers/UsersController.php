<?php

namespace App\Http\Controllers;

use App\Models\ProductFamily;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    private $users;
    private $roles;
    private $families;

    public function __construct()
    {
        $this->users = User::all();
        $this->roles = Role::all();
        $this->families = ProductFamily::all();
    }

    public function index()
    {
        seo()->title('Gestión Acosta');

        return view('users', [
            'users' => $this->users,
        ]);
    }

    public function create()
    {
        seo()->title('Gestión Acosta');

        return view('users', [
            'users' => $this->users,
            'roles' => $this->roles,
        ]);
    }

    public function edit(User $user)
    {
        seo()->title('Gestión Acosta');

        return view('users', [
            'users' => $this->users,
            'userToEdit' => $user,
            'roles' => $this->roles,
        ]);
    }

    public function view(User $user, Request $request)
    {

        if ($request->export) {
            return $this->export($user);
        }

        seo()->title('Gestión Acosta');

        return view('timers', [
            'users' => $this->users,
            'userToEdit' => $user,
            'roles' => $this->roles,
            'trackings' => $user->trackings,
            'families' => $this->families,
            'total' => $user->trackings->sum('time'),
        ]);
    }

    private function export($user)
    {
        $filename = 'tracking.csv';

        $handle = fopen('php://output', 'w');

        fputcsv($handle, ['Nombre', $user->name]);
        fputcsv($handle, ['E-mail', $user->email]);
        fputcsv($handle, ['Horas por mes', $user->hours_per_month]);
        fputcsv($handle, ['Salario', $user->salary]);
        fputcsv($handle, []);

        fputcsv($handle, ['Familia', 'Tarea', 'Tiempo']);

        foreach ($user->trackings as $tracking) {
            fputcsv(
                $handle,
                [
                    $tracking->task->family->name ?? 'Sin familia',
                    $tracking->task->name  ?? 'Sin tarea',
                    gmdate("H:i:s", $tracking->time)
                ]
            );
        }

        fputcsv($handle, ['', 'Total', gmdate("H:i:s", $user->trackings->sum('time'))]);

        fclose($handle);

        return response('', 200)->header('Content-Type', 'text/csv')->header('Content-Disposition', 'attachment; filename="' . $filename . '"');
    }

    public function delete(User $user)
    {
        // No se puede eliminar a sí mismo
        if ($user->id == auth()->user()->id) {
            return redirect()->back();
        }

        // No se puede eliminar al administrador
        if ($user->id == 1) {
            return redirect()->back();
        }

        $user->delete();

        return redirect()->back()->with('message', 'Se ha borrado con éxito con éxito');
    }

    public function save(Request $request)
    {

        if ($request->id) {
            $user = User::find($request->id);
        } else {
            $user = new User;
        }

        $user->fill([
            'name' => $request->name,
            'email' => $request->email,
            'salary' => $request->salary,
            'hours_per_month' => $request->hours_per_month,
        ]);

        if ($request->password != '') {
            $user->fill([
                'password' => Hash::make($request->password),
            ]);
        }

        $user->save();

        $user->assignRole($request->role);

        return redirect()->back()->with('message', 'Se ha guardado con éxito');
    }
}
