<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Trabajador']);

        DB::table('users')->insert([
            'name' => 'Admin Befresh',
            'email' => 'admin@befresh.es',
            'password' => Hash::make('admin123'),
        ]);

        $user = User::find(1);
        $user->assignRole('Admin');

        DB::table('users')->insert([
            'name' => 'Trabajador 1',
            'email' => 'worker@befresh.es',
            'password' => Hash::make('trabajador123'),
        ]);

        $user = User::find(2);
        $user->assignRole('Trabajador');


    }
}
